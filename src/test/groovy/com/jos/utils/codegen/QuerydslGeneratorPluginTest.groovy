package com.jos.utils.codegen

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

/**
 * @since 1.0.0
 */
class QuerydslGeneratorPluginTest {
    private Project project
    private QuerydslGeneratorTask task
    private QuerydslGeneratorPluginExtension extension


    @BeforeMethod
    void setup() {
        project = ProjectBuilder.builder().build()
        project.plugins.apply(QuerydslGeneratorPlugin.class)

        extension = project.extensions.querydsl as QuerydslGeneratorPluginExtension

        task = project.tasks.generateQueryDSL as QuerydslGeneratorTask
    }


    @Test
    void testPluginInvoke() {

//        extension.userName = 'zetafin-dev'
//        password = '123456'
//        jdbcUrl = 'jdbc:mysql://dev.zetafin.cn:3306/zetafin?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&useSSL=false'
//        generateSrc = "$project.projectDir/src/main/java/

        extension.driverClass = 'com.mysql.jdbc.Driver'
        extension.userName = 'root'
        extension.password = '123456'
        extension.jdbcUrl = 'jdbc:mysql://127.0.0.1:3306/test?autoReconnect=true&useUnicode=yes&characterEncoding=UTF-8&allowMultiQueries=true&useSSL=false&useInformationSchema=true'
        extension.domainSrcPath = "/tmp/generated/src/main/java/"
        extension.domainPackageName = 'com.wicc.commons.biz.domain'
        extension.repositorySrcPath = "/tmp/generated/src/main/kotlin/"
        extension.repositoryPackageName = 'com.wicc.commons.biz.repository'
        extension.serviceSrcPath = "/tmp/generated/src/main/kotlin/"
        extension.servicePackageName = 'com.wicc.commons.biz.service'
        extension.controllerSrcPath = "/tmp/generated/src/main/kotlin/"
        extension.controllerPackageName = 'com.wicc.commons.controller'
        task.run()
    }

}
