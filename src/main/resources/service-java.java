package $servicePackage;

import $domainPackage.$domainName;

public interface ${domainName}Service {
    public ${domainName} getById(Long id);

    public ${domainName} save(${domainName} ${domainNameSmall});
}
