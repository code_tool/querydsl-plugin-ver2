package $repositoryPackage;

import $domainPackage.$domainName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface  ${domainName}Repository extends JpaRepository<${domainName}, Long>,
        QuerydslPredicateExecutor<${domainName}>{
}