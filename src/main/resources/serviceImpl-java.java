package ${servicePackage}.impl;

import ${servicePackage}.${domainName}Service;
import $domainPackage.$domainName;
import $repositoryPackage.${domainName}Repository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
class ${domainName}ServiceImpl implements ${domainName}Service {

    @Override
    public ${domainName} getById(Long id) {
        return ${domainNameSmall}Repository.getOne(id);
    }

    @Override
    public ${domainName} save(${domainName} ${domainNameSmall}) {
        return ${domainNameSmall}Repository.saveAndFlush(${domainNameSmall});
    }

    @Autowired
    private ${domainName}Repository ${domainNameSmall}Repository;
}
