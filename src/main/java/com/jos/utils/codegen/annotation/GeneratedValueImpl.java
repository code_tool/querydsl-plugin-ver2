package com.jos.utils.codegen.annotation;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.lang.annotation.Annotation;

/**
 * Created by zhangxinglin on 2017/7/12.
 */
public class GeneratedValueImpl implements GeneratedValue {

    @Override
    public GenerationType strategy() {
        return GenerationType.IDENTITY;
    }

    @Override
    public String generator() {
        return null;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return GeneratedValue.class;
    }
}
