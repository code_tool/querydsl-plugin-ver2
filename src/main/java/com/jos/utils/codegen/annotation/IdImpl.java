package com.jos.utils.codegen.annotation;

import javax.persistence.Id;
import java.lang.annotation.Annotation;

/**
 * Created by zhangxinglin on 2017/7/12.
 */
public class IdImpl implements Id {
    @Override
    public Class<? extends Annotation> annotationType() {
        return Id.class;
    }
}

