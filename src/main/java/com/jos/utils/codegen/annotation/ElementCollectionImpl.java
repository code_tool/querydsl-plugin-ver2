package com.jos.utils.codegen.annotation;

import javax.persistence.ElementCollection;
import javax.persistence.FetchType;
import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * Created by zhangxinglin on 2017/7/12.
 */
public class ElementCollectionImpl implements ElementCollection {

    @Override
    public Class targetClass() {
        return Map.class;
    }

    @Override
    public FetchType fetch() {
        return FetchType.EAGER;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return ElementCollection.class;
    }
}
