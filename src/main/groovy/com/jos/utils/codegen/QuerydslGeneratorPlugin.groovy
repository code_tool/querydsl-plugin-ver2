package com.jos.utils.codegen

import org.gradle.api.Plugin
import org.gradle.api.Project

class QuerydslGeneratorPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.extensions.create("querydsl", QuerydslGeneratorPluginExtension)
        QuerydslGeneratorTask generateTask = project.tasks.create("generateQueryDSL", QuerydslGeneratorTask.class)
    }
}
