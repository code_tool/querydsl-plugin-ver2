package com.jos.utils.codegen

class QuerydslGeneratorPluginExtension {
    String driverClass
    String userName
    String password
    String jdbcUrl
    String domainSrcPath
    String domainPackageName
    String repositorySrcPath
    String repositoryPackageName
    String serviceSrcPath
    String servicePackageName
    String controllerSrcPath
    String controllerPackageName
    boolean exportForeignKeys = false
    String excludeTableNamePattern
    String includeTableNamePattern
}
