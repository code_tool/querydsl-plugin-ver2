package com.jos.utils.codegen

import com.querydsl.codegen.JavaTypeMappings
import com.querydsl.sql.Configuration
import com.querydsl.sql.MySQLTemplates
import com.querydsl.sql.SQLTemplates
import com.querydsl.sql.codegen.MetaDataSerializer
import org.gradle.api.DefaultTask
import org.gradle.api.Task
import org.gradle.api.tasks.TaskAction

import java.sql.Driver

class QuerydslGeneratorTask extends DefaultTask {

    @TaskAction
    def run() {
        String driverClass = project.querydsl.driverClass
        String userName = project.querydsl.userName
        String password = project.querydsl.password
        String jdbcUrl = project.querydsl.jdbcUrl
        String domainSrcPath = project.querydsl.domainSrcPath
        String domainPackageName = project.querydsl.domainPackageName
        String repositorySrcPath = project.querydsl.repositorySrcPath
        String repositoryPackageName =  project.querydsl.repositoryPackageName
        String serviceSrcPath = project.querydsl.serviceSrcPath
        String servicePackageName =  project.querydsl.servicePackageName
        String controllerSrcPath = project.querydsl.controllerSrcPath
        String controllerPackageName =  project.querydsl.controllerPackageName

        boolean exportForeignKeys = project.querydsl.exportForeignKeys
        String excludeTableNamePattern = project.querydsl.excludeTableNamePattern
        String includeTableNamePattern = project.querydsl.includeTableNamePattern

        Driver driver = Class.forName(driverClass).newInstance()
        Properties properties = new Properties()
        properties.setProperty("user", userName)
        properties.setProperty("password", password)
        properties.setProperty("remarks", "true");

        def connection = driver.connect(jdbcUrl, properties)
        def exporter = new MetaDataExporterExt()
        exporter.setPackageName(domainPackageName)
        exporter.setRepositorySrcPath(repositorySrcPath)
        exporter.setRepositoryPackageName(repositoryPackageName)
        exporter.setServiceSrcPath(serviceSrcPath)
        exporter.setServicePackageName(servicePackageName)
        exporter.setControllerSrcPath(controllerSrcPath)
        exporter.setControllerPackageName(controllerPackageName)
        File outputDir = new File(domainSrcPath)
        Task javaCompileTask = project.tasks.getByName("compileJava")
        javaCompileTask.source outputDir
        exporter.setTargetFolder(outputDir)
        exporter.setSerializerClass(MetaDataSerializer.class)
//        exporter.setBeanSerializerClass(BeanSerializer.class)
        exporter.setBeanSerializerClass(EntityBeanSerializer.class)

        SQLTemplates templates = new MySQLTemplates()

        Configuration configuration = new Configuration(templates)
//        configuration.registerType("json", Map.class)
        configuration.registerType("json", String.class)
        configuration.registerType("datetime", Date.class)
        configuration.registerType("date", Date.class)

        exporter.setExportPrimaryKeys(true)
        exporter.setExportForeignKeys(exportForeignKeys)
        exporter.setConfiguration(configuration)
        exporter.setTypeMappings(new JavaTypeMappings())
        exporter.setExcludeTableNamePattern(excludeTableNamePattern)
        exporter.setIncludeTableNamePattern(includeTableNamePattern)
        exporter.setColumnAnnotations(true)
//        exporter.setValidationAnnotations(true)

        exporter.export(connection.metaData)

        connection.close()
    }
}
